import 'package:flutter/material.dart';

class Fixture {
  String name;
  int address;
  int numberOfChannel;
  Icon fixtureIcon;

  Fixture({this.name, this.address, this.numberOfChannel});

  Fixture.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        address = json['address'],
        numberOfChannel = json['numberOfChannel'];

  Map<String, dynamic> toJson() => {
        'name': name,
        'address': address,
        'numberOfChannel': numberOfChannel,
      };


  @override
  String toString() {
    return "name: " +
        "$name, " +
        "address: " +
        "$address, " +
        "number of channel: " +
        "$numberOfChannel";
  }
}
