import 'package:dmx_light_controller/addNewFixturesScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

import 'bluetoothOfScreen.dart';
import 'findDeviceScreen.dart';
import 'helper.dart';

// This widget is the root of our application.
class BLEDMXController extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: Helper.appTitle,
      theme: ThemeData(
        primaryColor: Colors.blue,
      ),
      home: StreamBuilder<BluetoothState>(
        stream: FlutterBlue.instance.state,
        initialData: BluetoothState.unknown,
        builder: (c, snapshot) {
          final state = snapshot.data;
          if (state == BluetoothState.on) {
            return FindDevicesScreen();
          }
          return BluetoothOffScreen(
            state: state,
          );
        },
      ),
    );
  }
}
