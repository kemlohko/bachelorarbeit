import 'dart:convert';
import 'package:flutter/material.dart';

import 'fixture.dart';
import 'helper.dart';
import 'navigationDrawer.dart';

class EditFixturesScreen extends StatefulWidget {
  @override
  _EditFixturesScreen createState() => _EditFixturesScreen();
}

class _EditFixturesScreen extends State<EditFixturesScreen> {
  List<Widget> createExpansionTilesOfFixtures(List<Fixture> fixtures) {
    List<Fixture> toBeSaveInPrefs = fixtures;
    List<Widget> expansionTileFixturesList = [];
    fixtures.forEach((fixture) {
      TextEditingController editFixtureName = new TextEditingController();
      TextEditingController editFixtureAddress = new TextEditingController();
      TextEditingController editFixtureNumberOfChannel =
          new TextEditingController();

      ExpansionTile expansionTile = new ExpansionTile(
        title: Text(
          fixture.name,
          style: TextStyle(color: Colors.white),
        ),
        maintainState: true,
        leading: Icon(
          Icons.devices,
          size: 30,
          color: Colors.white,
        ),
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Column(children: <Widget>[
              TextField(
                keyboardType: TextInputType.name,
                controller: editFixtureName,
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                  hintStyle: TextStyle(color: Colors.white),
                  labelStyle: TextStyle(color: Colors.grey),
                  labelText: "Name",
                  hintText: fixture.name,
                ),
              ),
              TextField(
                keyboardType: TextInputType.number,
                controller: editFixtureAddress,
                decoration: InputDecoration(
                  hintStyle: TextStyle(color: Colors.white),
                  labelStyle: TextStyle(color: Colors.grey),
                  labelText: "Address",
                  hintText: fixture.address.toString(),
                ),
              ),
              TextField(
                keyboardType: TextInputType.number,
                controller: editFixtureNumberOfChannel,
                decoration: InputDecoration(
                  hintStyle: TextStyle(color: Colors.white),
                  labelStyle: TextStyle(color: Colors.grey),
                  labelText: "Number of channel",
                  hintText: fixture.numberOfChannel.toString(),
                ),
              ),
            ]),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              RaisedButton(
                child: Text("Delete"),
                onPressed: () {
                  setState(() {
                    toBeSaveInPrefs.remove(fixture);
                    Helper.saveFixturesInPrefs(jsonEncode(toBeSaveInPrefs));
                  });
                },
              ),
              RaisedButton(
                child: Text("Save"),
                onPressed: () {
                  saveFixture(fixtures, fixture, editFixtureName,
                      editFixtureAddress, editFixtureNumberOfChannel);
                },
              ),
            ],
          ),
        ],
      );
      expansionTileFixturesList.add(expansionTile);
    });

    return expansionTileFixturesList;
  }

  void saveFixture(
      List<Fixture> fixtures,
      Fixture fixtureToEdit,
      TextEditingController editFixtureName,
      TextEditingController editFixtureAddress,
      TextEditingController editFixtureNumberOfChannel) {
    setState(() {
      (editFixtureName.text.isNotEmpty)
          ? fixtureToEdit.name = editFixtureName.text
          : fixtureToEdit.name = fixtureToEdit.name;
      (editFixtureAddress.text.isNotEmpty)
          ? fixtureToEdit.address = int.parse(editFixtureAddress.text)
          : fixtureToEdit.address = fixtureToEdit.address;
      (editFixtureNumberOfChannel.text.isNotEmpty)
          ? fixtureToEdit.numberOfChannel =
              int.parse(editFixtureNumberOfChannel.text)
          : fixtureToEdit.numberOfChannel = fixtureToEdit.numberOfChannel;
      Helper.saveFixturesInPrefs(jsonEncode(fixtures));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Helper.appTitle),
        backgroundColor: Colors.black38,
      ),
      backgroundColor: Colors.blueGrey.shade900,
      drawer: NavigationDrawer(),
      body: FutureBuilder<List<Fixture>>(
        future: Helper.getFixturesFromPrefs(),
        initialData: [],
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView(
              children: [...createExpansionTilesOfFixtures(snapshot.data)],
            );
          }
          return Container();
        },
      ),
    );
  }
}
