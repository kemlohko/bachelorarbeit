import 'package:flutter/material.dart';

import 'helper.dart';
import 'navigationDrawer.dart';

class PlayOrDeleteScenesScreen extends StatefulWidget {
  @override
  _PlayOrDeleteScenesScreen createState() => _PlayOrDeleteScenesScreen();
}

class _PlayOrDeleteScenesScreen extends State<PlayOrDeleteScenesScreen> {
  List<Widget> createExpansionTileForScenes(List<String> scenes) {
    List<Widget> expansionScenes = [];
    scenes.forEach((scene) {
      ExpansionTile expansionTile = new ExpansionTile(
        title: Text(
          scene,
          style: TextStyle(color: Colors.white),
        ),
        maintainState: true,
        leading: Icon(
          Icons.play_circle_fill,
          color: Colors.grey,
          size: 30,
        ),
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              RaisedButton(
                child: Text('Delete'),
                onPressed: () {
                  setState(() {
                    scenes.remove(scene);
                    Helper.saveScenesInPrefs(scenes);
                    Helper.writeToCharacteristic(key: 'del', value: scene);
                  });
                },
              ),
              RaisedButton(
                child: Text('Play'),
                onPressed: () {
                  Helper.writeToCharacteristic(key: 'read', value: scene);
                },
              )
            ],
          ),
        ],
      );

      expansionScenes.add(expansionTile);
    });

    return expansionScenes;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Helper.appTitle),
        backgroundColor: Colors.black38,
      ),
      drawer: NavigationDrawer(),
      backgroundColor: Colors.blueGrey.shade900,
      body: FutureBuilder<List<String>>(
        future: Helper.readScenesFromPrefs(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data.isNotEmpty) {
            return ListView(
              children: <Widget>[
                ...createExpansionTileForScenes(snapshot.data)
              ],
            );
          }
          return Center(
            child: Text(
              'No scenes found!',
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
          );
        },
      ),
    );
  }
}
