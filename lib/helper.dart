import 'dart:convert';
import 'fixture.dart';
import 'dart:io';

import 'package:flutter_blue/flutter_blue.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Helper {
  static BluetoothDevice bluetoothDevice;
  static BluetoothService bluetoothService;

  static final String appTitle = 'BLE DMX Controller';
  static final String fixtureKey = 'fixtures';
  static final String sceneKey = 'scenes';
  static final String characteristicUUID =
      '6e400002-b5a3-f393-e0a9-e50e24dcca9e';
  static final String serviceUUID = '6E400001-B5A3-F393-E0A9-E50E24DCCA9E';

  static void writeToCharacteristic(
      {int channel,
      int valueOnChangeStart,
      int valueOnChangeEnd,
      String key,
      var value}) async {
    for (BluetoothCharacteristic characteristic
        in bluetoothService.characteristics) {
      if (characteristic.uuid == Guid(characteristicUUID)) {
        if (Platform.isIOS) {
          // send dmx data to gatt server
          if (channel != null &&
              valueOnChangeStart != null &&
              valueOnChangeEnd != null)
            await characteristic.write(utf8.encode('$channel' +
                ',' +
                '$valueOnChangeStart' +
                ',' +
                '$valueOnChangeEnd'));

          // send other data
          else
            await characteristic.write(utf8.encode(key + ',' + '$value'));
        }
        if (Platform.isAndroid) {
          if (channel != null &&
              valueOnChangeStart != null &&
              valueOnChangeEnd != null)
            await characteristic.write(
                utf8.encode('$channel' +
                    ',' +
                    '$valueOnChangeStart' +
                    ',' +
                    '$valueOnChangeEnd'),
                withoutResponse: true);
          else
            await characteristic.write(utf8.encode(key + ',' + '$value'),
                withoutResponse: true);
        }
      }
    }
  }

  static Future<List<Fixture>> getFixturesFromPrefs() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<Fixture> fixtures = [];
    String data = prefs.getString(fixtureKey);
    if (data != null) {
      List<dynamic> jsonData = jsonDecode(data);
      jsonData.forEach((element) {
        fixtures.add(new Fixture.fromJson(element));
      });
    }
    return fixtures;
  }

  static void saveFixturesInPrefs(String fixture) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(fixtureKey, fixture);
  }

  static void saveScenesInPrefs(List<String> scenes) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList(sceneKey, scenes);
  }

  static Future<List<String>> readScenesFromPrefs() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> values = prefs.getStringList(sceneKey);
    if (values == null) values = [];

    return values;
  }
}
