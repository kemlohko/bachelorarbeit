import 'package:dmx_light_controller/findDeviceScreen.dart';
import 'package:flutter/material.dart';

import 'homeScreen.dart';
import 'addNewFixturesScreen.dart';
import 'editFixturesScreen.dart';
import 'helper.dart';
import 'playOrDeleteScenesScreen.dart';

class NavigationDrawer extends StatelessWidget {
  NavigationDrawer();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Center(
              child: Text(
                'DMX Menu',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                    fontWeight: FontWeight.bold),
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.blueGrey.shade900,
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('HOME'),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => HomeScreen()));
            },
          ),
          ListTile(
            leading: Icon(
              Icons.add,
              size: 30,
            ),
            title: Text('ADD FIXTURE'),
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return AddNewFixturesScreen();
              }));
            },
          ),
          ListTile(
            leading: Icon(Icons.create),
            title: Text('EDIT FIXTURES'),
            onTap: () async {
              Navigator.pop(context);
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return EditFixturesScreen();
              }));
            },
          ),
          ListTile(
            leading: Icon(Icons.play_circle_fill),
            title: Text('PLAY SCENE'),
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return PlayOrDeleteScenesScreen();
              }));
            },
          ),
          ListTile(
            leading: Icon(Icons.bluetooth_disabled),
            title: Text('DISCONNECT'),
            onTap: () async {
              Navigator.pop(context);
              Helper.writeToCharacteristic(key: 'dis', value: '');
              await Helper.bluetoothDevice.disconnect();
              Navigator.of(context)
                  .pushReplacement(MaterialPageRoute(builder: (context) {
                //return ConnectToRaspberryPi();
                return FindDevicesScreen();
              }));
            },
          ),
        ],
      ),
    );
  }
}
