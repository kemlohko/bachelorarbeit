import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

import 'fixture.dart';
import 'fader.dart';
import 'helper.dart';
import 'navigationDrawer.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen();

  @override
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  List<Fader> _createListOfFaderForOneFixture(Fixture fixture) {
    List<Fader> faderList = [];
    double value = 0.0;
    for (int index = 1; index <= fixture.numberOfChannel; index++) {
      Fader mySlider = new Fader(value, index, fixture.address);
      faderList.add(mySlider);
    }
    return faderList;
  }

  List<Widget> createExpansionTilesOfFixtures(List<Fixture> fixtures) {
    List<Widget> expansionTileFixturesList = [];
    fixtures.forEach((fixture) {
      ExpansionTile expansionTile = new ExpansionTile(
        title: Text(
          fixture.name,
          style: TextStyle(color: Colors.white),
        ),
        maintainState: true,
        leading: Icon(
          Icons.devices,
          size: 30,
          color: Colors.grey,
        ),
        children: <Widget>[..._createListOfFaderForOneFixture(fixture)],
      );
      expansionTileFixturesList.add(expansionTile);
    });
    return expansionTileFixturesList;
  }

  showDialogBoxForSavingScene() {
    TextEditingController sceneName = new TextEditingController();
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: Text('Save the scene'),
            content: TextField(
              controller: sceneName,
              decoration: InputDecoration(
                  hintText: 'Scene name', border: OutlineInputBorder()),
            ),
            actions: [
              FlatButton(
                child: Text('Cancel'),
                onPressed: () => Navigator.of(context).pop(),
              ),
              FlatButton(
                child: Text('save'),
                onPressed: () async {
                  if (sceneName.text.isNotEmpty) {
                    Helper.writeToCharacteristic(
                        key: 'save', value: sceneName.text);
                    List<String> scenes = await Helper.readScenesFromPrefs();
                    scenes.add(sceneName.text);
                    Helper.saveScenesInPrefs(scenes);
                    Navigator.of(context).pop();
                  }
                },
              )
            ],
          );
        });
  }

  void calculateAndSendTotalNumberOfChannel(List<Fixture> fixtures) {
    int totalNumberOfChannel = 0;
    fixtures.forEach((fixture) {
      totalNumberOfChannel = totalNumberOfChannel + fixture.numberOfChannel;
    });
    Helper.writeToCharacteristic(key: 'size', value: totalNumberOfChannel);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey.shade900,
      appBar: AppBar(
        title: Text(Helper.appTitle),
        backgroundColor: Colors.black38,
        actions: <Widget>[
          FlatButton(
            child: Text(
              "BLACKOUT",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            onPressed: () {
              setState(() {
                Helper.writeToCharacteristic(key: 'blackout', value: '');
              });
            },
          ),
        ],
      ),
      drawer: NavigationDrawer(),
      floatingActionButton: FloatingActionButton.extended(
        label: Text('create scene'),
        tooltip: 'create a scene',
        backgroundColor: Colors.black38,
        icon: Icon(Icons.add),
        onPressed: () => showDialogBoxForSavingScene(),
      ),
      body: FutureBuilder<List<Fixture>>(
        future: Helper.getFixturesFromPrefs(),
        initialData: [],
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data.isNotEmpty) {
            final List<Fixture> fixtures = snapshot.data;
            calculateAndSendTotalNumberOfChannel(fixtures);
            return ListView(
              children: <Widget>[...createExpansionTilesOfFixtures(fixtures)],
            );
          }
          return Center(
            child: Text(
              'no fixtures found!',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
          );
        },
      ),
    );
  }
}
