import 'package:flutter/material.dart';
import 'helper.dart';

// ignore: must_be_immutable
class Fader extends StatefulWidget {
  Fader(this.value, this.index, this.fixtureAddress);

  final int index;
  final int fixtureAddress;
  double value;

  @override
  _Fader createState() => _Fader();
}

class _Fader extends State<Fader> {
  int valueOnChanged = 0;
  int valueOnChangedStart = 0;
  bool black = true;

  @override
  Widget build(BuildContext context) {
    // The index always start by 1 for each fixture. We then need to determinate
    // the right channel based on the index and the fixture address
    int channel = widget.index + widget.fixtureAddress - 1;
    int index = widget.index;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "$index",
          style: TextStyle(color: Colors.white),
        ),
        Container(
          width: 300,
          child: Slider(
            min: 0.0,
            max: 255.0,
            value: widget.value,
            onChangeStart: (value) {
              valueOnChangedStart = value.toInt();
            },
            onChanged: (value) async {
              setState(() {
                widget.value = value;
              });
            },
            onChangeEnd: (valueOnChangeEnd) {
              Helper.writeToCharacteristic(
                  channel: channel,
                  valueOnChangeStart: valueOnChangedStart,
                  valueOnChangeEnd: valueOnChangeEnd.toInt());
            },
          ),
        ),
        Text(
          "${widget.value.toInt()}",
          style: TextStyle(color: Colors.white),
        )
      ],
    );
  }
}
