import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

import 'dart:convert';

import 'homeScreen.dart';
import 'fixture.dart';
import 'helper.dart';

class SetUniverseScreen extends StatefulWidget {
  SetUniverseScreen(this.bluetoothDevice);

  final BluetoothDevice bluetoothDevice;

  @override
  _SetUniverseScreen createState() => _SetUniverseScreen();
}

class _SetUniverseScreen extends State<SetUniverseScreen> {
  final textEditingControllerFixtureName = TextEditingController();
  final textEditingControllerFixtureAddress = TextEditingController();
  final textEditingControllerFixtureNumberOfChannel = TextEditingController();
  List<Fixture> fixtures = [];

  void navigateToHomeScreen(List<Fixture> fixtures) async {
    int numberOfChannel = 0;
    // calculate the total number of used channel by all the devices
    fixtures.forEach((fixture) {
      numberOfChannel = numberOfChannel + fixture.numberOfChannel;
    });
    Helper.saveFixturesInPrefs(jsonEncode(fixtures));
    final List<BluetoothService> bluetoothService =
        await widget.bluetoothDevice.discoverServices();
    widget.bluetoothDevice.services;
    bluetoothService.forEach((service) {
      if (service.uuid == Guid(Helper.serviceUUID)) {
        Helper.bluetoothService = service;
        Helper.bluetoothDevice = widget.bluetoothDevice;
        Helper.writeToCharacteristic(key: 'size', value: numberOfChannel);
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return HomeScreen();
        }));
      }
    });
  }

  void addFixture() {
    String fixtureName = textEditingControllerFixtureName.value.text;
    int address = int.parse(textEditingControllerFixtureAddress.value.text);
    int numberOfChannel =
        int.parse(textEditingControllerFixtureNumberOfChannel.value.text);

    // check that the fixture name already exist
    Fixture fixture = new Fixture(
        name: fixtureName, address: address, numberOfChannel: numberOfChannel);
    if (!fixtures.contains(fixture)) {
      fixtures.add(fixture);
      textEditingControllerFixtureName.clear();
      textEditingControllerFixtureAddress.clear();
      textEditingControllerFixtureNumberOfChannel.clear();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Helper.appTitle),
        backgroundColor: Colors.black38,
      ),
      backgroundColor: Colors.blueGrey.shade900,
      body: FutureBuilder<List<Fixture>>(
        future: Helper.getFixturesFromPrefs(),
        builder: (context, snapshot) {
          if (snapshot.data == null || snapshot.data.isEmpty) {
            return Center(
              child: Container(
                width: 300,
                height: 500,
                child: Card(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 20, 0, 50),
                        child: Center(
                          child: Text("ADD FIXTURES",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 25)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          children: [
                            TextField(
                              controller: textEditingControllerFixtureName,
                              decoration:
                                  InputDecoration(hintText: " fixture name"),
                            ),
                            TextField(
                              controller: textEditingControllerFixtureAddress,
                              decoration:
                                  InputDecoration(hintText: "dmx address"),
                            ),
                            TextField(
                              controller:
                                  textEditingControllerFixtureNumberOfChannel,
                              decoration: InputDecoration(
                                  hintText: "number of channel"),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 30),
                            child: RaisedButton(
                              child: Text("ADD FIXTURE"),
                              color: Colors.blue,
                              onPressed: () {
                                setState(() {
                                  addFixture();
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                      Visibility(
                        visible: fixtures.length == 0 ? false : true,
                        child: IconButton(
                            icon: Icon(
                              Icons.forward_rounded,
                              size: 50,
                              color: Colors.blue,
                            ),
                            onPressed: () => navigateToHomeScreen(fixtures)),
                      )
                    ],
                  ),
                ),
              ),
            );
          } else
            navigateToHomeScreen(snapshot.data);

          return Container();
        },
      ),
    );
  }
}
