import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

import 'setUniverseScreen.dart';
import 'helper.dart';

class FindDevicesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Colors.blueGrey.shade900,
        appBar: AppBar(
          title: Text(Helper.appTitle),
          backgroundColor: Colors.black38,
        ),
        body: RefreshIndicator(
          onRefresh: () =>
              FlutterBlue.instance.startScan(timeout: Duration(seconds: 4)),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                StreamBuilder<List<ScanResult>>(
                  stream: FlutterBlue.instance.scanResults,
                  initialData: [],
                  builder: (c, snapshot) => Column(
                    children: snapshot.data
                        .map(
                          (r) => ListTile(
                            leading: Icon(
                              Icons.devices,
                              color: Colors.white,
                            ),
                            title: Text(
                              (r.device.name.isNotEmpty)
                                  ? r.device.name
                                  : r.device.id.toString(),
                              style: TextStyle(color: Colors.white),
                            ),
                            onTap: () => Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) {
                                r.device.connect();
                                return StreamBuilder<BluetoothDeviceState>(
                                  stream: r.device.state,
                                  initialData:
                                      BluetoothDeviceState.disconnected,
                                  builder: (c, snapshot) {
                                    final state = snapshot.data;
                                    if (state == BluetoothDeviceState.connected)
                                      return SetUniverseScreen(r.device);
                                    return FindDevicesScreen();
                                  },
                                );
                              }),
                            ),
                          ),
                        )
                        .toList(),
                  ),
                ),
              ],
            ),
          ),
        ),
        floatingActionButton: StreamBuilder<bool>(
          stream: FlutterBlue.instance.isScanning,
          initialData: false,
          builder: (c, snapshot) {
            if (snapshot.data) {
              return FloatingActionButton(
                child: Icon(Icons.stop),
                onPressed: () => FlutterBlue.instance.stopScan(),
                backgroundColor: Colors.red,
              );
            } else {
              return FloatingActionButton(
                  child: Icon(Icons.search),
                  backgroundColor: Colors.black38,
                  onPressed: () => FlutterBlue.instance
                      .startScan(timeout: Duration(seconds: 4)));
            }
          },
        ),
      ),
    );
  }
}
