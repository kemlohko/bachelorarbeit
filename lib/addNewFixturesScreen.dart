import 'dart:convert';

import 'package:flutter/material.dart';

import 'fixture.dart';
import 'navigationDrawer.dart';
import 'helper.dart';

List<Fixture> fixtures = [];

class AddNewFixturesScreen extends StatelessWidget {
  final TextEditingController textEditingFixtureName =
      new TextEditingController();
  final TextEditingController textEditingFixtureAddress =
      new TextEditingController();
  final TextEditingController textEditingFixtureMode =
      new TextEditingController();

  void addNewFixture() {
    String name = textEditingFixtureName.value.text;
    int address = int.parse(textEditingFixtureAddress.value.text);
    int numberOfChannel = int.parse(textEditingFixtureMode.value.text);

    Fixture fix = new Fixture(
        name: name, address: address, numberOfChannel: numberOfChannel);

    if (!fixtures.contains(fix)) {
      fixtures.add(fix);
      Helper.saveFixturesInPrefs(json.encode(fixtures));
      textEditingFixtureName.clear();
      textEditingFixtureAddress.clear();
      textEditingFixtureMode.clear();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Helper.appTitle),
        backgroundColor: Colors.black38,
      ),
      drawer: NavigationDrawer(),
      backgroundColor: Colors.blueGrey.shade900,
      body: FutureBuilder<List<Fixture>>(
        future: Helper.getFixturesFromPrefs(),
        builder: (context, snapshot) {
          fixtures = snapshot.data;
          // if (snapshot.hasData && snapshot.data.length == 0) {
          return Center(
            child: Container(
              width: 300,
              height: 400,
              color: Colors.grey.shade300,
              child: Card(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 50),
                      child: Center(
                        child: Text("ADD NEW FIXTURE",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 25)),
                      ),
                    ),
                    TextField(
                      controller: textEditingFixtureName,
                      decoration: InputDecoration(hintText: "Name"),
                    ),
                    TextField(
                      controller: textEditingFixtureAddress,
                      decoration: InputDecoration(hintText: "Address"),
                    ),
                    TextField(
                      controller: textEditingFixtureMode,
                      decoration:
                          InputDecoration(hintText: "Number of channel"),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 30),
                          child: RaisedButton(
                            child: Text(
                              "ADD FIXTURE",
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Colors.blue,
                            onPressed: () {
                              addNewFixture();
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
          //}

          // return Container();
        },
      ),
    );
  }
}
