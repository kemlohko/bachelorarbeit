# BLEDMXontroller

An app to control dmx light over BLE.

## Getting Started
1- First clone the project.


2- Transfer the folder "gatt-server-files" on your raspberry pi.


3- Start the server by running "python3 gatt-server.py"


4- Run the app on your smartphone and scan for BLE devices.


