import dbus
import interfaces
import application
import service
import characteristic
import advertisement

import dbus.mainloop.glib

from gi.repository import GLib

mainloop = None
BLUEZ_SERVICE_NAME = 'org.bluez'
SERVICE_UUID = '6e400001-b5a3-f393-e0a9-e50e24dcca9e'
RX_CHARACTERISTIC_UUID = '6e400002-b5a3-f393-e0a9-e50e24dcca9e'
LOCAL_NAME = 'rpi-gatt-server'


def register_app_cb():
    print('GATT application registered')


def register_app_error_cb(error):
    print('Failed to register application: ' + str(error))
    mainloop.quit()


def register_ad_cb():
    print('Advertisement registered')


def register_ad_error_cb(error):
    print('Failed to register advertisement: ' + str(error))
    mainloop.quit()


def find_adapter(bus):
    remote_om = dbus.Interface(bus.get_object(BLUEZ_SERVICE_NAME, '/'),
                                    interfaces.DBUS_OM_IFACE)
    objects = remote_om.GetManagedObjects()

    for o, props in objects.items():
        if interfaces.GATT_MANAGER_IFACE in props.keys():
            return o

    return None


def main():
    global mainloop

    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    bus = dbus.SystemBus()

    adapter = find_adapter(bus)
    if not adapter:
        print('GattManager1 interface not found')
        return

    service_manager = dbus.Interface(
        bus.get_object(BLUEZ_SERVICE_NAME, adapter),
        interfaces.GATT_MANAGER_IFACE)

    ad_manager = dbus.Interface(bus.get_object(BLUEZ_SERVICE_NAME, adapter),
                                interfaces.LE_ADVERTISING_MANAGER_IFACE)
    
    app = application.Application(bus)
    ser = service.Service(bus, 0, SERVICE_UUID, True)
    charac = characteristic.Characteristic(bus, 0, RX_CHARACTERISTIC_UUID, ['write'], ser)

    app.add_service(ser)
    ser.add_characteristic(charac)

    adv = advertisement.Advertisement(bus, 0, 'peripheral')
    adv.add_service_uuid(SERVICE_UUID)
    adv.add_local_name(LOCAL_NAME)
    adv.include_tx_power = True
    
    mainloop = GLib.MainLoop()

    print('Registering GATT application...')

    service_manager.RegisterApplication(app.get_path(), {},
                                        reply_handler=register_app_cb,
                                        error_handler=register_app_error_cb)
    

    ad_manager.RegisterAdvertisement(adv.get_path(), {},
                                     reply_handler=register_ad_cb,
                                     error_handler=register_ad_error_cb)


    mainloop.run()
    


if __name__ == '__main__':
    main()
