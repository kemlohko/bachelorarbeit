import dbus
import dbus.service
import interfaces
import exceptions
import controller
import threading


class Characteristic(dbus.service.Object):
    """
    org.bluez.GattCharacteristic1 interface implementation
    """

    def __init__(self, bus, index, uuid, flags, service):
        self.path = service.path + '/char' + str(index)
        self.bus = bus
        self.uuid = uuid
        self.service = service
        self.flags = flags
        self.descriptors = []
        dbus.service.Object.__init__(self, bus, self.path)

    def get_properties(self):
        return {
            interfaces.GATT_CHRC_IFACE: {
                'Service': self.service.get_path(),
                'UUID': self.uuid,
                'Flags': self.flags,
                'Descriptors': dbus.Array(
                    self.get_descriptor_paths(),
                    signature='o')
            }
        }

    def get_path(self):
        return dbus.ObjectPath(self.path)

    def add_descriptor(self, descriptor):
        self.descriptors.append(descriptor)

    def get_descriptor_paths(self):
        result = []
        for desc in self.descriptors:
            result.append(desc.get_path())
        return result

    def get_descriptors(self):
        return self.descriptors

    @dbus.service.method(interfaces.DBUS_PROP_IFACE,
                         in_signature='s',
                         out_signature='a{sv}')
    def GetAll(self, interface):
        if interface != interfaces.GATT_CHRC_IFACE:
            raise exceptions.InvalidArgsException()

        return self.get_properties()[interfaces.GATT_CHRC_IFACE]


    @dbus.service.method(interfaces.GATT_CHRC_IFACE, in_signature='aya{sv}')
    def WriteValue(self, value, options):
        print(format(bytearray(value).decode()))
        decoded_value = format(bytearray(value).decode())
        #ble_data = decoded_value.split(',')
        thread = threading.Thread(target=controller.main, args=(decoded_value,))
        thread.start()
        thread.join()
        print('finish')
        raise exceptions.NotSupportedException()

    
