from array import array

from ola.ClientWrapper import ClientWrapper
from ola.DMXConstants import DMX_MIN_SLOT_VALUE, DMX_MAX_SLOT_VALUE, \
    DMX_UNIVERSE_SIZE

import time

dmxDataSize = 512
UNIVERSE = 1
wrapper = None
dmx_data = array('B', [0] * dmxDataSize)


def DmxSent(state):
    wrapper.Stop()


def updateDmx(number_of_test):
    global wrapper
    wrapper = ClientWrapper()
    client = wrapper.Client()
    program_time = 0
    for i in range(1, number_of_test + 1):
        start_time = time.time()
        for j in range(1, 255):
            dmx_data[1] = j
            client.SendDmx(UNIVERSE, dmx_data, DmxSent)
            end_time = time.time()
            execution_time = end_time - start_time
            program_time = program_time + execution_time
            print('test:', i, ' time:', execution_time)

    avg_time = program_time / number_of_test
    print(avg_time)

    wrapper.Run



if __name__ == '__main__':
    updateDmx(10000)
