from array import array

from ola.ClientWrapper import ClientWrapper
from ola.DMXConstants import DMX_MIN_SLOT_VALUE, DMX_MAX_SLOT_VALUE, \
    DMX_UNIVERSE_SIZE

import time
import subprocess

__author__ = 'Alex Kemloh'

"""
This script had multiple functions for sending dmx data, saving a scene
reading a scene, deleting a scene, stoping ola daemon, 
setting the dmx data size and blackout the dmx data

"""

dmxDataSize = 512
UNIVERSE = 1
wrapper = None
dmx_data = array('B', [0] * dmxDataSize)


"""
That is the main function of the programm.
This function send the dmx data to the ola daemon and it run until we stop the wrapper.
Make also sure wer stop the wrapper !!!!
"""

def updateDmx(data):
    global wrapper
    wrapper = ClientWrapper()
    client = wrapper.Client()

    if isinstance(data, list):
        channel = int(data[0]) - 1
        startValue = int(data[1])
        endValue = int(data[2])

        if startValue < endValue:
            for i in range(startValue, endValue + 1):
                dmx_data[channel] = i
                print(dmx_data)
                client.SendDmx(UNIVERSE, dmx_data, DmxSent)
                

        else:
            for i in reversed(range(endValue, startValue
                                    )):
                dmx_data[channel] = i
                print(dmx_data)
                client.SendDmx(UNIVERSE, dmx_data, DmxSent)


    else:
        client.SendDmx(UNIVERSE, data, DmxSent)


    wrapper.Run()  # run until we stop the wrapper


# stop the wrapper
def DmxSent(state):
    wrapper.Stop()


    
def saveScene(name):
    stringValues = []
    for value in dmx_data:
        stringValues.append(str(value) + ',')
    with open('scenes/' + name + '.txt', 'w') as scene:
        scene.writelines(stringValues)


def readScene(name):
    data = array('B', [])
    try:
        with open('scenes/' + name + '.txt') as scene:
            values = scene.read().split(',')
            for line in values:
                if line != '':
                    data.append(int(line))
            updateDmx(data)
    except:
        print('scene don\'t exist')


def deleteScene(name):
    subprocess.run('rm scenes/{}.txt'.format(name), shell=True)


def blackout():
    global dmx_data
    dmx_data = array('B', [0] * dmxDataSize)
    updateDmx(dmx_data)


def setDmxDataSize(size):
    global dmx_data
    global dmxDataSize
    dmx_data = array('B', [0] * size)
    dmxDataSize = size


'''
 The dmx protocol don't define how to stop sending dmx data.
 In this method i just reset the data and restart the ola server.
'''


def disconnect():
    # Reset all data
    global dmx_data
    global dmxDataSize
    dmxDataSize = 512
    dmx_data = array('B', [0] * dmxDataSize)
    subprocess.run('sudo service olad restart', shell=True)

'''
Entry point of the programm.
'''
def main(decoded_value):
    data = decoded_value.split(',')
    firstValue = data[0]

    if firstValue == 'save':
        saveScene(data[1])
    elif firstValue == 'read':
        readScene(data[1])
    elif firstValue == 'del':
        deleteScene(data[1])
    elif firstValue == 'size':
        setDmxDataSize(int(data[1]))
    elif firstValue == 'blackout':
        blackout()
    elif firstValue == 'dis':
        disconnect()
    else:
        updateDmx(data)
